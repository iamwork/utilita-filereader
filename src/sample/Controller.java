package sample;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;


public class Controller {
    @FXML
    public TextArea TxtInfo;
    @FXML
    public TextArea TxtDecision;
    @FXML
    public TextField TxtFileExport;
    @FXML
    public TextField LogMsg;
    @FXML
    public TextField TxtSeach;
    @FXML
    public ComboBox cmbInpNames;
    @FXML
    public ComboBox cmdSeach;
    @FXML
    public Button ExportFile;
    @FXML
    public Button ImportFile;
    @FXML
    public Button Seach;
    @FXML
    public Button Clean;
    @FXML
    public SplitPane spltTextW;

    ObservableList<String> filesNames = FXCollections.observableArrayList();
    ObservableList<String> filesSeach = FXCollections.observableArrayList("");
    List<String> records = new ArrayList<>();
    String path, file_s, file_in, file_out;
    String prop_name = "app.properties";

    @FXML
    public void initialize() throws IOException {

        btnOff(ImportFile);
        btnOff(ExportFile);
        btnOff(Seach);
        btnOff(Clean);
        TxtSeach.setText(" ");
        records.add(" ");
        TxtInfo.setWrapText(true);
        path = new File(".").getCanonicalPath();
        file_s = (path + "/Settings/").replace("\\", "/");
        file_in = (path + "/Load/").replace("\\", "/");
        file_out = (path + "/Unload/").replace("\\", "/");
        TxtFileExport.setText("DataOutput.csv");

        final File dir_s = new File(file_s);

        if (!dir_s.exists()) {
            dir_s.mkdirs();
            setStatusMsg("Создана дирректория Settings для загрузки файлов");
        }

        final File dir_in = new File(file_in);

        if (!dir_in.exists()) {
            dir_in.mkdirs();
            setStatusMsg("Создана дирректория Load для загрузки файлов");
        }

        final File dir_out = new File(file_out);

        if (!dir_out.exists()) {
            dir_out.mkdirs();
            setStatusMsg("Создана дирректория Unload для загрузки файлов");
        }

        File dir = new File(file_in); // указывает на директорию

        for (File e : dir.listFiles()) {
            if (e.isFile()) {
                filesNames.add(e.getName());
            }
        }
        cmbInpNames.setItems(filesNames);

        if (filesNames.size() > 0) {
            cmbInpNames.setValue(filesNames.get(0));
        } else cmbInpNames.setValue(" - ");

        String settingsSeach = file_s + prop_name;

        List<String> prop = readProperties(settingsSeach, "seach");

        if (prop.size() > 0) {
            filesSeach.clear();
        }

        filesSeach.addAll(prop);
        cmdSeach.setItems(filesSeach);
        cmdSeach.setValue(filesSeach.get(0));
    }

    List<String> readProperties(String file_name, String propRead) throws IOException {

        List<String> prop = new ArrayList<>();

        if (Files.exists(Paths.get(file_name))) {
            byte[] readIn = Files.readAllBytes(Paths.get(file_name));
            String replacer = new String(readIn, StandardCharsets.UTF_8);

            try (StringReader inp = new StringReader(replacer)) {
                Properties appProps = new Properties();
                appProps.load(inp);
                String readData = appProps.getProperty(propRead);
                String list[] = readData.split(",");
                for (int i = 0; i < list.length; i++) {
                    prop.add(list[i]);
                }
            }
        }
        return prop;
    }

    Boolean ifContainesSimbol(String str, String word) {

        if (str.contains(word)) {
            return true;
        }
        return false;
    }

    public List<String[]> readAll(Reader reader) {

        List<String[]> list = null;
        try (CSVReader csvReader = new CSVReader(reader);) {
            list = csvReader.readAll();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void readCSV() {

        String fileDir = (file_in + cmbInpNames.getValue());
        List<String[]> list = new ArrayList<>();
        List<String> line_data = new ArrayList<>();
        List<String> lines_all = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(fileDir), "UTF-8"));) {

            String str;
            StringBuilder sb = new StringBuilder();

            CSVParser parser = new CSVParserBuilder()
                    .withSeparator(';')
                    .withIgnoreQuotations(false)
                    .build();


            CSVReader csvReader = new CSVReaderBuilder(reader)
                    .withSkipLines(0)
                    .withCSVParser(parser)
                    .build();

            String[] line;
            while ((line = csvReader.readNext()) != null) {
                list.add(line);

                for (int i = 0; i < line.length; i++) {
                    line_data.add(line[i].replaceAll("[\\s\\p{Z}]+", " ").trim());
                }
                lines_all.add(String.valueOf(line_data));
                line_data.clear();
            }

            reader.close();
            csvReader.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String addChar(String str, String ch, int position) {
        return str.substring(0, position) + ch + str.substring(position);
    }

    public void onImportFile() {

        btnOn(ImportFile);
        TxtInfo.clear();
        String fileDir = (file_in + cmbInpNames.getValue());
        String data;
        boolean key = false;
        // проверка файла на наличие

        if (Files.exists(Paths.get(fileDir))) {

            try (BufferedReader in = new BufferedReader(new InputStreamReader(
                    new FileInputStream(fileDir), "UTF-8"));) {

                String str;
                StringBuilder sb = new StringBuilder();

                while ((str = in.readLine()) != null) {
                    sb.append(str.replaceAll("[\\s\\p{Z}]+", " ").toLowerCase().trim());
                }

                String[] subStr;
                String delimeter = ";"; // Разделитель
                subStr = sb.toString().split(delimeter);
                // Вывод результата на экран
                for (int i = 0; i < subStr.length; i++) {
                    records.add(subStr[i] + ";" + "\n");
                }
                records.add("\n");
                String sep = "";
                for (int i = 0; i < spltTextW.getWidth() / 9; i++) {
                    sep += "-";
                }
                TxtInfo.appendText(addChar(sep, " 24 ", sep.length() / 2) + "\n");
                TxtInfo.appendText(String.valueOf(records));

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else setStatusMsg("Файл: " + Paths.get(fileDir) + " - не обнаружен!");

    }

    public void onExportFile() {

        btnOn(ExportFile);
        String fileDir = (file_out + TxtFileExport.getText());

        if (TxtDecision.getText().length() > 0) {
            try (BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(fileDir), "windows-1251"))) {
                out.write(TxtDecision.getText());
            } catch (UnsupportedEncodingException | FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else setStatusMsg("Отсутствуют данные для выгрузки в файл!");
    }

    public void setStatusMsg(String msg) {

        LogMsg.setText(" " + msg);
    }

    public void onSeach() {

        btnOn(Seach);
        LogMsg.setText("");
        TxtDecision.clear();
        TxtDecision.setText(" ");

        if (records.size() > 0) {

            if (!TxtSeach.getText().equals(" ")) {

                for (String l : records) {
                    if (ifContainesSimbol(l.toLowerCase(), TxtSeach.getText().toLowerCase().trim())) {
                        TxtDecision.appendText(l + "\n ");
                    }
                }
                if (TxtDecision.getText().length() == 0) {
                    TxtDecision.appendText("Не найдено!");
                }

            } else setStatusMsg("Заполните наименование для поиска!");

        } else setStatusMsg("Выберите и загрузите файл!");
    }

    private boolean btnOn(Button btn) {

        btn.setStyle(" -fx-background-color: \n" +
                "#ffffff,\n" +
                "linear-gradient( #daeaf1 20%,  #abebc5 20%, #ffffff 10%, #ffffff 10%,  #abebc5 20%,  #daeaf1 20%);");

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                btnOff(btn);
            }
        }, 1000);

        return true;
    }

    private boolean btnOff(Button btn) {

        btn.setStyle(" -fx-background-color: \n" +
                "#ffffff,\n" +
                "linear-gradient(#a3cadb 20%,  #daeaf1 20%, #ffffff 10%, #ffffff 10%, #daeaf1 20%,  #a3cadb 20%);"
        );

        return false;
    }

    public void onClean() {
        LogMsg.setText("");
        TxtInfo.clear();
        records.clear();
        TxtDecision.clear();
        TxtDecision.setText("");
    }

}
